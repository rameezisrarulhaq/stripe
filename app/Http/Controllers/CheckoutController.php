<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class CheckoutController extends RegisterController
{


    /**
     * display the checkout page
     */
    public function index(){

        $stripe = app('stripe');

        return view('billing.card', [
            'stripe' => $stripe
        ]);
    }
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
       \Log::info($request);
       \Log::info($request->hiddenInputPayment);
       return;

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        try {
            $user->createAsStripeCustomer(); // this will successfully create a customer on stripe

            //$intent = $user->createSetupIntent();

            \Log::info('User card intent');

            \Log::info($intent);

        } catch (Exception $e) {
            \Log::info($e);

            return false;
        }



        $this->guard()->login($user);

        //return redirect('home');

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
