<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>


    {{--  <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">

    <link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700\|Material+Icons" />  --}}

    
   
    {{--  <link rel="stylesheet" href="{{ mix('/css/vuetify.css') }}">  --}}

</head>

<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif

        <div class="container v-application">

            <div id="app">


                <h1 class=" text-center">
                    GREL/EOE/ABUDO Billing Module
                </h1>

                <v-divider></v-divider>
                

               <checkout inline-template :stripe="'{{ $stripe }}'">

              
                    <v-form ref="form" v-model="valid" lazy-validation @submit.prevent="submit">
                      <div class="plan">
                        <h2>Your Master Plan Billing Cycle</h2>
                      </div>
                  
                      <div class="profile">
                        <h2>Create Your Profile Info</h2>
                        <v-text-field v-model="name" :counter="10" :rules="nameRules" label="Name" required></v-text-field>
                  
                        <v-text-field v-model="email" :rules="emailRules" label="E-mail" required></v-text-field>
                  
                        <v-text-field
                          type="password"
                          :rules="passwordRules"
                          v-model="password"
                          label="Password"
                          hint="At least 8 characters"
                         
                          required
                        ></v-text-field>
                  
                        <v-text-field
                          type="password"
                          :rules="passwordRules"
                          v-model="password_confirmation"
                          label="Confirm Password"
                          required
                        ></v-text-field>
                      </div>
                  
                      <div class="billing">
                        <h2>Enter your billing info</h2>
                  
                        <v-row>
                          <v-col cols="12" md="4">
                            <v-text-field
                              v-model="cardHolderName"
                              :rules="cardHolderNameRules"
                              label="Cardholder's Name"
                              required
                            ></v-text-field>
                          </v-col>
                  
                          <v-col cols="12" md="4">
                            <v-text-field v-model="cardNumber" :rules="cardNumberRules" label="Card Number" required></v-text-field>
                          </v-col>

                          <v-col cols="4" md="1">
                            <v-text-field v-model="cvc" :rules="cvcRules" label="CVC" required></v-text-field>
                          </v-col>
                  
                          <v-col cols="4" md="1">
                            <v-text-field
                              v-model="expiryMonth"
                              :rules="expiryMonthRules"
                              label="Expiry Month"
                              required
                            ></v-text-field>
                          </v-col>
                          <v-col cols="4" md="1">
                            <v-text-field v-model="expiryYear" :rules="expiryYearRules" label="Expiry Year" required></v-text-field>
                          </v-col>
                        </v-row>
                      </div>
                  
                      <v-checkbox
                        v-model="checkbox"
                        :rules="[v => !!v || 'You must agree to continue!']"
                        label="Do you agree?"
                        required
                      ></v-checkbox>
                  
                      <v-btn :disabled="!valid" color="success" class="mr-4" @click="validate">Subscribe</v-btn>
                  
                      <v-btn color="error" class="mr-4" @click="reset">Reset Form</v-btn>
                  
                      <v-btn color="warning" @click="resetValidation">Reset Validation</v-btn>
                    </v-form>

               </checkout>


               <form action="/charge" method="post" id="payment-form">
                <div class="form-row">
                <label for="card-element">
                    Credit or debit card
                </label>
                <div id="card-element">
                    <!-- A Stripe Element will be inserted here. -->
                </div>
    
                <!-- Used to display form errors. -->
                <div id="card-errors" role="alert"></div>
                </div>
    
                <button>Submit Payment</button>
            </form>
      

            </div>

        </div>
    </div>

    <script src="{{ mix('/js/app.js')}}"></script>

    <script src="https://js.stripe.com/v3/"></script>
  
    <script>

      const stripe = Stripe('pk_test_rgoEfLdEjclfkiXSdeBV3FxL');

              
        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
                } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
                console.log(result);
                
                }
            });
            
            //pass the payment method to the serveR
            //so you can do something like
            //$user->addPaymentMethod($paymentMethod);
             stripe.createPaymentMethod('card', card, {
              }).then(function(result) {
                console.log(result);
              });
              
              
              stripe.createSource(card).then(function(result) {
                console.log(result)
              });

        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
             var form = document.getElementById('payment-form');
             var hiddenInput = document.createElement('input');
             hiddenInput.setAttribute('type', 'text');
             hiddenInput.setAttribute('name', 'stripeToken');
             hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);
            
            alert('Success! Got token: ' + token.id);

            stripe.createToken(card).then(function(result) {
              //console.log(result);
             // alert(result);
            });
          }


    </script>



</body>

</html>